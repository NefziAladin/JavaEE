package tn.esprit.EasyMission.Business;

import javax.ejb.Stateless;

import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.Entity.User;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceLocal;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceRemote;
import tn.esprit.EasyMission.iBusiness.IUserServiceLocal;
import tn.esprit.EasyMission.iBusiness.IUserServiceRemote;

@Stateless
public class UserService extends BasicOpsImpl<User> implements IUserServiceLocal ,IUserServiceRemote {

}
