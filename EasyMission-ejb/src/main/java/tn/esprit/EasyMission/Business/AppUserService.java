package tn.esprit.EasyMission.Business;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.iBusiness.IAppUserServiceRemote;


@Stateless
@LocalBean
public class AppUserService implements IAppUserServiceRemote{
	@PersistenceContext(unitName="EasyMission-ejb")
	EntityManager em;

	@Override
	public int ajouterUser(AppUser u) {
		em.persist(u);
		return u.getId();
	}

	@Override
	public void updateUser(AppUser u) {
		em.merge(u);
	}

	@Override
	public void removeUser(int usId) {
		 em.remove(em.find(AppUser.class, usId));
	}
	

	@Override
	public List<AppUser> getUsers() {
		TypedQuery<AppUser> q = em.createQuery("SELECT u FROM AppUser u",AppUser.class);
		return q.getResultList();
	}

	@Override
	public List<String> getAllUserNamesJPQL() {
		TypedQuery<String> q = em.createQuery("SELECT u.nom FROM AppUser u",String.class);
		return q.getResultList();
	}

	@Override
	public AppUser getUsersByEmailAndPassword(String email, String password) {
		TypedQuery<AppUser> query = em.createQuery("select u from AppUser u where u.email =:email and u.password =:password", AppUser.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		AppUser us = null;
		
		try{
			us = query.getSingleResult();
		} catch (NoResultException e){
			System.out.println("Auccun user trouve avec email : "+ email);
		}
		return us;
	}

	@Override
	public AppUser getUsersById(int userId) {
		TypedQuery<AppUser> q = em.createQuery("SELECT u FROM AppUser u where u.id =:userId ",AppUser.class);
		q.setParameter("userId",userId );
		return q.getSingleResult();
	}

	@Override
	public AppUser authentification(String username, String pwd) {
		
		String jpql = "SELECT z FROM AppUser z WHERE z.nom=:username and z.password=:pwd";
		Query query = em.createQuery(jpql);
		query.setParameter("username", username);
		query.setParameter("pwd", pwd);
		AppUser user= null;
		try {
		  user=(AppUser)query.getSingleResult();

	
} catch (Exception e) {
	// TODO: handle exception
}

		return user;	
	}
}
