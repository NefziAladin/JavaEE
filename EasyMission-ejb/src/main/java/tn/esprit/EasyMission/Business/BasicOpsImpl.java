package tn.esprit.EasyMission.Business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.EasyMission.iBusiness.IBasicOpsLocal;



@Stateless
public class BasicOpsImpl<T> implements IBasicOpsLocal<T>  {
	@PersistenceContext
	protected EntityManager entityManager;
	
	
	public BasicOpsImpl() {
		super();
	}

	@Override
	public void add(T t) {
		
		entityManager.persist(t);
	}

	@Override
	public void update(T t) {
		entityManager.merge(t);
		
	}

	@Override
	public void update(Class<T> entityClass, Integer id) {
		T t = this.findById(entityClass, id);
		entityManager.merge(t);
		
	}

	@Override
	public List<T> findAll(Class<T> entityClass) {
		String sql = "FROM " + entityClass.getName();
		Query query = entityManager.createQuery(sql);
		return query.getResultList();
	}
	@Override
	public T findById(Class<T> entityClass, Integer id) {
		return entityManager.find(entityClass, id);
	}

	@Override
	public void remove(T t) {
		entityManager.remove(t);
		
	}

	@Override
	public void removeById(Class<T> entityClass, Integer id) {
		entityManager.remove(this.findById(entityClass, id));
		
	}

}
