package tn.esprit.EasyMission.Business;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.Entity.Appliement;
import tn.esprit.EasyMission.Entity.ApplimentPK;
import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.iBusiness.IAppliment;

@Stateless
@LocalBean
public class ApplimentService implements IAppliment {

	@PersistenceContext(unitName = "EasyMission-ejb")
	EntityManager em;

	@Override
	public void ajouterReservation(int idJob, int userId, Date dateReservation) {
		ApplimentPK apk = new ApplimentPK(userId, idJob);
		Appliement r = new Appliement(apk, dateReservation);
		em.persist(r);

	}

	@Override
	public Appliement verifierAppliement(JobVolenteer jobVolenteer, AppUser user) {
		TypedQuery<Appliement> query = em.createQuery(
				"Select r from Appliement r where r.job =:jobVolenteer and r.user =:user", Appliement.class);

		query.setParameter("jobVolenteer", jobVolenteer);
		query.setParameter("user", user);
		
		
		Appliement appliement = null;

		try {
			appliement = query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("error");
		}	
		return appliement;
	}

	@Override
	public Map<AppUser,Integer> getBestVolenteerUser() {
		//String sql ="SELECT c FROM Appliement c GROUP BY c.user.id HAVING COUNT(c.user.id)=(SELECT MAX(COUNT(c2.user.id)) FROM Appliement c2)";
		Map<AppUser,Integer> map = new LinkedHashMap<>();
		int	idUser = 0;
		int count = 0;
		String sql= "SELECT c.applimentPK.userId, count(*) as a FROM Appliement c GROUP BY c.applimentPK.userId HAVING count(*) = ALL (SELECT count(*) FROM Appliement b  GROUP BY b.applimentPK.userId)";
		Query query = this.em.createQuery(sql);
		List<Object[]> list =query.getResultList();

		for (Object[] objects : list) {
	
		 count=Integer.parseInt(objects[1].toString());
		
		idUser=Integer.parseInt(objects[0].toString());
	System.out.println(idUser);

	
		
		}
		query=em.createQuery("select user  from AppUser user where user.id=:id",AppUser.class  );
		query.setParameter("id", idUser);
	AppUser	user=(AppUser) query.getSingleResult();
	AppUser u=new AppUser();
	u.setId(idUser);
	u.setPrenom(user.getPrenom());
	u.setNom(user.getNom());
	System.out.println(u.toString());
		
	System.out.println("vvvvv");
map.put(u, count);
	
	
		return map;
	}

	@Override
	public Map<JobVolenteer,Integer> getBestJobByParticipation() {
		Map<JobVolenteer,Integer> map = new LinkedHashMap<>();
		int	jobId = 0;
		int count = 0;
		String sql= "SELECT c.applimentPK.jobId, count(*) as a FROM Appliement c GROUP BY c.applimentPK.jobId HAVING count(*) = ALL (SELECT count(*) FROM Appliement b  GROUP BY b.applimentPK.jobId)";
		Query query = this.em.createQuery(sql);
		List<Object[]> list =query.getResultList();

		for (Object[] objects : list) {
	
		 count=Integer.parseInt(objects[1].toString());
		System.out.println(count);
		jobId=Integer.parseInt(objects[0].toString());
	System.out.println(jobId);
	query=em.createQuery("select job  from JobVolenteer job where job.idJobV=:id",JobVolenteer.class  );
	query.setParameter("id", jobId);
	JobVolenteer	job=(JobVolenteer) query.getSingleResult();


map.put(job, count);
	
		
		}
		
	
	
		return map;
	}

	@Override
	public List<JobVolenteer> allparticipation() {
		//List<JobVolenteer> jobVolenteer;
String sql ="select c from JobVolenteer c" ;
Query query =em.createQuery(sql);
return query.getResultList();
		}

	@Override
	public boolean verifUserResOrNot(int JobId, int userId) {
		TypedQuery<Appliement> query = em.createQuery("Select r from Appliement r where r.job.idJobV  =:JobId and r.user.id  =:userId" , Appliement.class);
		
		
		query.setParameter("JobId", JobId);
		query.setParameter("userId", userId);
		
		Appliement appliement = null ;
		
		try {
			appliement = query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("error");
		}
		
		if (appliement != null) {
			return true;
		}

		else {
			return false;
		}

}

	@Override
	public void annulerReservation(int trainingId, int userId) {
		ApplimentPK apk = new ApplimentPK(userId, trainingId);
		em.remove(em.find(Appliement.class,apk));
		
	}

	
	
	}
