package tn.esprit.EasyMission.Business;

import java.util.List;
import java.util.Properties;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceLocal;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceRemote;


@LocalBean
@Stateless
public class JobVolenteerService extends BasicOpsImpl<JobVolenteer> implements IJobVolenteerServiceLocal ,IJobVolenteerServiceRemote{

	@PersistenceContext
	private EntityManager em;
	
	Query query;
	@Override
	public List<JobVolenteer> SearchJobVolenteer(String parametre) {
		
		List<JobVolenteer> jobVolenteer;

		query = em.createQuery(
				"select  p from JobVolenteer p where  p.Titre like CONCAT( :param ,'%') or p.Lieu like CONCAT( :param ,'%')or p.Description like CONCAT( :param ,'%')or p.Taches like CONCAT( :param ,'%')  ",
				JobVolenteer.class);

		query.setParameter("param", parametre);

		jobVolenteer = query.getResultList();

		return jobVolenteer;
	}
	@Override
	public List<JobVolenteer> getAll() {
	List<JobVolenteer> liste=em.createQuery("select job from JobVolenteer job",JobVolenteer.class).getResultList();
		return liste;
	}
	@Override
	public void send() {
		
		
			final String username = "ahmed.ben.othmenne@gmail.com";
			final String password = "123abz123";
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			Message message = new MimeMessage(session);

			try {
				message.setFrom(new InternetAddress("ahmed.ben.othmenne@gmail.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("chamseddine.chouikh@gmail.com"));

				message.setSubject("From SGA");

				message.setText("Der sr we found that you without contarct and wi suggest many type of policy ");

				Transport.send(message);

				System.out.println("Done");

			} catch (MessagingException ex) {

			

		}
}
	}
