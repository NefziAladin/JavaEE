package tn.esprit.EasyMission.iBusiness;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.EasyMission.Entity.JobVolenteer;

@Local

public interface IJobVolenteerServiceLocal extends IBasicOpsLocal<JobVolenteer> {
	public List<JobVolenteer> SearchJobVolenteer(String parametre);
	public List<JobVolenteer> getAll();
	public void send();
	}
