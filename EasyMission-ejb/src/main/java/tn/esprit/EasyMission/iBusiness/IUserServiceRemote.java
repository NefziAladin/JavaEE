package tn.esprit.EasyMission.iBusiness;

import javax.ejb.Remote;

import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.Entity.User;

@Remote
public interface IUserServiceRemote extends IBasicOpsRemote<User>{

}
