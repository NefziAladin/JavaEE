package tn.esprit.EasyMission.iBusiness;

import javax.ejb.Local;

import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.Entity.User;

@Local
public interface IUserServiceLocal extends IBasicOpsRemote<User> {

}
