package tn.esprit.EasyMission.iBusiness;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.Entity.Appliement;
import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.Entity.User;

@Remote
public interface IAppliment <Appliement> {

	public void ajouterReservation(int idJob, int userId, Date dateAppliement) ;
	
	public Appliement verifierAppliement (JobVolenteer jobVolenteer,AppUser user ) ;
	public boolean verifUserResOrNot(int trainingId, int userId);
	public void annulerReservation(int trainingId, int userId);
	public void cancelReservation(int trainingId);
	
	
	public Map<AppUser,Integer> getBestVolenteerUser();

	public Map<JobVolenteer,Integer> getBestJobByParticipation();
	public List<JobVolenteer> allparticipation();

}
