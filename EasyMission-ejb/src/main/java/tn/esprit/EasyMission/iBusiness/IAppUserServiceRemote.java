package tn.esprit.EasyMission.iBusiness;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.EasyMission.Entity.AppUser;

@Remote
public interface IAppUserServiceRemote {
	public int ajouterUser(AppUser u);
	public void updateUser(AppUser u);
	public void removeUser(int usId);
	public AppUser getUsersById(int userId);
	public List<AppUser> getUsers();
	public List<String> getAllUserNamesJPQL();
	public AppUser getUsersByEmailAndPassword(String email, String password);
	public AppUser authentification(String username, String pwd);
}
