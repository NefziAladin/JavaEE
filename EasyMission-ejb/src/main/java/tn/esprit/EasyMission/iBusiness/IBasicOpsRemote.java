package tn.esprit.EasyMission.iBusiness;

import java.util.List;

public interface IBasicOpsRemote<T> {
	public void add(T t);
	
	public void update(T t);
	
	public void update(Class<T> entityClass, Integer id);
	
	public List<T> findAll(Class<T> entityClass);
	
	public T findById(Class<T> entityClass, Integer id);
	
	public void remove(T t);
	
	public void removeById(Class<T> entityClass, Integer id);

}
