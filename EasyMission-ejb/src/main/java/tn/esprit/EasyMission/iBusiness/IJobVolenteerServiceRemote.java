package tn.esprit.EasyMission.iBusiness;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.EasyMission.Entity.JobVolenteer;

@Remote
public interface IJobVolenteerServiceRemote extends IBasicOpsRemote<JobVolenteer> {
	public void send();


}
