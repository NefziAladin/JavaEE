package tn.esprit.EasyMission.Entity;


import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;



/**
 * Entity implementation class for Entity: JobVolenteer
 *
 */
@Entity

public class JobVolenteer implements Serializable {


	

	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idJobV;
	private String Titre;
	private Date DateDebut;
	private Date DateFin;
	private String Lieu;
	private String Description;
	private String Taches;
	
	private String image ;
	
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@OneToMany(mappedBy="job",fetch = FetchType.EAGER)
	private List<Appliement> appliements;
	
	
	
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	

	public List<Appliement> getAppliements() {
		return appliements;
	}
	public void setAppliements(List<Appliement> appliements) {
		this.appliements = appliements;
	}
	public JobVolenteer(String titre) {
		super();
		Titre = titre;
	}
	public JobVolenteer() {
		super();
	}   
	public Integer getIdJobV() {
		return this.idJobV;
	}

	public void setIdJobV(Integer idJobV) {
		this.idJobV = idJobV;
	}   
	public String getTitre() {
		return this.Titre;
	}

	public void setTitre(String Titre) {
		this.Titre = Titre;
	}   
	public Date getDateDebut() {
		return this.DateDebut;
	}

	public void setDateDebut(Date DateDebut) {
		this.DateDebut = DateDebut;
	}   
	public Date getDateFin() {
		return this.DateFin;
	}

	public void setDateFin(Date DateFin) {
		this.DateFin = DateFin;
	}   
	public String getLieu() {
		return this.Lieu;
	}

	public void setLieu(String Lieu) {
		this.Lieu = Lieu;
	}   
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}   
	public String getTaches() {
		return this.Taches;
	}

	public void setTaches(String Taches) {
		this.Taches = Taches;
	}
	@Override
	public String toString() {
		return "JobVolenteer [idJobV=" + idJobV + ", Titre=" + Titre + ", DateDebut=" + DateDebut + ", DateFin="
				+ DateFin + ", Lieu=" + Lieu + ", Description=" + Description + ", Taches=" + Taches + ", picture="
				 + ", image=" + image + ", appliements=" + appliements + "]";
	}
	
	
	
	
}
