package tn.esprit.EasyMission.Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Appliement implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ApplimentPK applimentPK ;
	private Date dateReservation;
	
	
	@ManyToOne
	@JoinColumn(name="userId",referencedColumnName="id",insertable=false,updatable=false)
	private AppUser user ;
	
	@ManyToOne
	@JoinColumn(name="jobId",referencedColumnName="idJobV",insertable=false,updatable=false)
	private JobVolenteer job ;
	
	
	
	

	public Appliement(ApplimentPK applimentPK, Date dateReservation) {
		super();
		this.applimentPK = applimentPK;
		this.dateReservation = dateReservation;
	}

	
	public Appliement() {
		super();
	}


	public ApplimentPK getApplimentPK() {
		return applimentPK;
	}

	public void setApplimentPK(ApplimentPK applimentPK) {
		this.applimentPK = applimentPK;
	}

	public Date getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}

	

	public AppUser getUser() {
		return user;
	}


	public void setUser(AppUser user) {
		this.user = user;
	}


	public JobVolenteer getJob() {
		return job;
	}

	public void setJob(JobVolenteer job) {
		this.job = job;
	}
	
	
	
	
}
