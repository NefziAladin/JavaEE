package tn.esprit.EasyMission.Entity;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class AppUser implements Serializable {
	
	
	@Override
	public String toString() {
		return "AppUser [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", phone=" + phone
				+ ", isActif=" + isActif + ", role=" + role + ", password=" + password + ", appliements=" + appliements
				+ "]";
	}
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	private String email;
	private int phone;
	private Boolean isActif;
	@Enumerated(EnumType.STRING)
	private Role role;
	private String password;
	@JsonBackReference
	@OneToMany(mappedBy="user")

	private List<Appliement> appliements;
	
	
	
	
	
	
	
	
	
	
	
	
	
	public AppUser() {
		super();
	}
	public List<Appliement> getAppliements() {
		return appliements;
	}
	public void setAppliements(List<Appliement> appliements) {
		this.appliements = appliements;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public Boolean getIsActif() {
		return isActif;
	}
	public void setIsActif(Boolean isActif) {
		this.isActif = isActif;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public AppUser(int id, String nom, String prenom, String email, int phone, Boolean isActif, Role role,
			String password) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.phone = phone;
		this.isActif = isActif;
		this.role = role;
		this.password = password;
	}
	public AppUser(String nom, String prenom, String email, int phone, Boolean isActif, Role role, String password) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.phone = phone;
		this.isActif = isActif;
		this.role = role;
		this.password = password;
	}
	
	

	
	
	
	
}
