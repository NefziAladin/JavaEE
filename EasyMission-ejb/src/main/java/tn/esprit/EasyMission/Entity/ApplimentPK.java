package tn.esprit.EasyMission.Entity;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class ApplimentPK implements Serializable{

	
	private static final long serialVersionUID = 1L;
		private int userId ;
		private int jobId ;
		
	
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + jobId;
			result = prime * result + userId;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ApplimentPK other = (ApplimentPK) obj;
			if (jobId != other.jobId)
				return false;
			if (userId != other.userId)
				return false;
			return true;
		}
		public ApplimentPK() {
			super();
		}
		public ApplimentPK(int userId, int jobId) {
			super();
			this.userId = userId;
			this.jobId = jobId;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public int getJobId() {
			return jobId;
		}
		public void setJobId(int jobId) {
			this.jobId = jobId;
		}
		
		
		
		
}
