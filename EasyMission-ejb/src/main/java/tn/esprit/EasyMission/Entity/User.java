package tn.esprit.EasyMission.Entity;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity(name="t_user")
public class User implements Serializable {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idUser;
	private String firstname;
	private String lastname;
	/*@OneToMany(mappedBy="user")
	private List<Appliement> appliements;*/
	private static final long serialVersionUID = 1L;
	
	

	
	public User() {
		super();
	} 
	//@ManyToMany(mappedBy="users")
	
	
	
	
	   
	public Integer getIdUser() {
		return this.idUser;
	}

	/*public List<Appliement> getAppliements() {
		return appliements;
	}




	public void setAppliements(List<Appliement> appliements) {
		this.appliements = appliements;
	}*/




	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}   
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}   
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
   
}
