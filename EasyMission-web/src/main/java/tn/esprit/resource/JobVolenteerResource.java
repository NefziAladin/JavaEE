package tn.esprit.resource;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tn.esprit.EasyMission.Business.BasicOpsImpl;
import tn.esprit.EasyMission.Business.JobVolenteerService;
import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceRemote;

@Path(value="Jobs")
@RequestScoped
public class JobVolenteerResource {
	
	@EJB
	JobVolenteerService jobsServiceRest ;
	
	
	
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRest()
	{
		
		return Response.ok(jobsServiceRest.getAll()).build(); 
	}
}
