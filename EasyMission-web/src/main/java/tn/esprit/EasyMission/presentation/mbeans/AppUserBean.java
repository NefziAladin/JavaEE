package tn.esprit.EasyMission.presentation.mbeans;

import java.io.Serializable;
import java.util.List;


import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import tn.esprit.EasyMission.Business.AppUserService;
import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.Entity.Role;

@ManagedBean
@ViewScoped
public class AppUserBean implements Serializable{

	private String prenom;
	private String nom;
	private String password;
	private String email;
	private int phone;
	private boolean isActif;
	private Role role;
	private List<AppUser> users;
	private Integer usIdToBeUpdated;
	
	

	@EJB
	AppUserService usService;



	
	public int addUser(){
		
		 return usService.ajouterUser(new AppUser(nom, prenom, email, phone, isActif, role, password));
	   
	}
	
	public List<AppUser> getUsers() {
		users = usService.getUsers();
		return users;
	}
	
	public void supprimer(Integer usId){
		usService.removeUser(usId);
	}
	
	public void modifier(AppUser us){
		this.setEmail(us.getEmail());
		this.setPassword(us.getPassword());
		this.setPhone(us.getPhone());
		this.setActif(us.getIsActif());
		this.setPrenom(us.getPrenom());
		this.setNom(us.getNom());
		this.setRole(us.getRole());
		this.setUsIdToBeUpdated(us.getId());
	//	String navigateTo = "/Welcome?faces-redirect=true";
	//	return navigateTo;
	}
	
	public void mettreAjourUser(){
		usService.updateUser(new AppUser(usIdToBeUpdated, nom, prenom, email, phone,isActif, role, password));
	}
	
	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public int getPhone() {
		return phone;
	}



	public void setPhone(int phone) {
		this.phone = phone;
	}



	public boolean isActif() {
		return isActif;
	}



	public void setActif(boolean isActif) {
		this.isActif = isActif;
	}



	public Role getRole() {
		return role;
	}



	public void setRole(Role role) {
		this.role = role;
	}



	public Integer getUsIdToBeUpdated() {
		return usIdToBeUpdated;
	}



	public void setUsIdToBeUpdated(Integer usIdToBeUpdated) {
		this.usIdToBeUpdated = usIdToBeUpdated;
	}



	public AppUserService getUsService() {
		return usService;
	}



	public void setUsService(AppUserService usService) {
		this.usService = usService;
	}

	public void setUsers(List<AppUser> users) {
		this.users = users;
	}
	
	
	
	
}
