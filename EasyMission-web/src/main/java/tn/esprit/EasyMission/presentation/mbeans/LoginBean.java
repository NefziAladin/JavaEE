package tn.esprit.EasyMission.presentation.mbeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.EasyMission.Business.AppUserService;
import tn.esprit.EasyMission.Business.ApplimentService;
import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.Entity.Role;

@ManagedBean
@SessionScoped
public class LoginBean {
	
	private String login;
	private String password;
	private static AppUser us;
	private boolean loggedIn;
	
	
	
	
	@EJB
	AppUserService usService;
	
	@EJB
    ApplimentService  applimentService	 ;
	
	public String doLogin(){
		String navigateTo = "null";
		us = usService.getUsersByEmailAndPassword(login, password);
		if (us != null && us.getRole() == Role.administrateur){
			navigateTo = "/pages/Register?faces-redirect=true";
		    loggedIn = true;
		} else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials"));
		}
	return navigateTo;

	}

	public String doLogout(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	    return "/pages/Login?faces-redirect=true";
	}
	
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public static AppUser getUs() {
		return us;
	}
	public void setUs(AppUser us) {
		this.us = us;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	

    public boolean resOrNot(int trainingId, int userId) {
    	return applimentService.verifUserResOrNot(trainingId, userId);
}
    
	
	 public void cancelReservation(int trainingId){
	  	  applimentService.annulerReservation(trainingId,LoginBean.getUs().getId());
	  	}
	
	
	
}
