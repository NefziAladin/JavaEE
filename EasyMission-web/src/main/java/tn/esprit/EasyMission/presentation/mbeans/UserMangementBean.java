package tn.esprit.EasyMission.presentation.mbeans;



import java.io.Serializable;

import javax.ejb.EJB;
//import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.iBusiness.IAppUserServiceRemote;


@ManagedBean
@SessionScoped
public class UserMangementBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	IAppUserServiceRemote basicOpsLocal ;
	
	private Boolean loggedInAsAgent = false;
	private AppUser user = new AppUser();
	private boolean isLogged;

	public String logout() {
		isLogged = false;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login?faces-redirect=true";
	}
	public String doLogin() {
		String navigateTo = "";
		AppUser userLoggedIn = basicOpsLocal.authentification(user.getNom(), user.getPassword());
		if (userLoggedIn != null) {
			isLogged = true;
			user = userLoggedIn;
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute("user", user);
			
			AppUser us1= (AppUser)session.getAttribute("user");
		//	System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooo "+us1.getFirstname());
			if (userLoggedIn instanceof AppUser) {
				navigateTo = "/iFrame/add?faces-redirect=true";
			}else {
				loggedInAsAgent = true;
				navigateTo = "/iFrame/showClient?faces-redirect=true";
			
				}
		} else {
			loggedInAsAgent = false;
			navigateTo = "erreur?faces-redirect=true";
		
			System.err.println("not");
		}
		return navigateTo;
	}
	
	
	
	public IAppUserServiceRemote getBasicOpsLocal() {
		return basicOpsLocal;
	}
	public void setBasicOpsLocal(IAppUserServiceRemote basicOpsLocal) {
		this.basicOpsLocal = basicOpsLocal;
	}
	public Boolean getLoggedInAsAgent() {
		return loggedInAsAgent;
	}
	public void setLoggedInAsAgent(Boolean loggedInAsAgent) {
		this.loggedInAsAgent = loggedInAsAgent;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	public boolean isLogged() {
		return isLogged;
	}
	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}


}
