package tn.esprit.EasyMission.presentation.mbeans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.SimpleFormatter;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import tn.esprit.EasyMission.Business.ApplimentService;
import tn.esprit.EasyMission.Business.JobVolenteerService;
import tn.esprit.EasyMission.Entity.AppUser;
import tn.esprit.EasyMission.Entity.Appliement;
import tn.esprit.EasyMission.Entity.JobVolenteer;
import tn.esprit.EasyMission.Entity.User;
import tn.esprit.EasyMission.iBusiness.IAppliment;
import tn.esprit.EasyMission.iBusiness.IJobVolenteerServiceLocal;
import tn.esprit.EasyMission.iBusiness.IUserServiceLocal;





@ManagedBean(name="adbean")
@SessionScoped
public class JobBean {
	
	
	@EJB
    ApplimentService  applimentService	 ;
	
	
	@EJB
	IJobVolenteerServiceLocal js ;
	
	@EJB
	IUserServiceLocal us ;
	
	@EJB
	IAppliment<Appliement> pew ;
	
	private Integer idJobV;
	private Integer idJobV1;

	private String Titre;
	private String Titre1;
	
	private Date DateDebut;
	private Date DateDebut1;

	private Date DateFin;
	private String Lieu;

	private String Lieu1;
	
private String Description;
private String Description1;

private String Taches;
private String Taches1;

private String picture;
private UploadedFile uploadedFile;

private UploadedFile ui;

	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	//session.setAttribute("user", user);

AppUser us1;

	private List<User> users ;
	
	private String text="" ;

	
	private List<JobVolenteer> jobs ;
	private List<User> oo = new ArrayList<User>();


	List<JobVolenteer> ll ;
	JobVolenteer j = new JobVolenteer();
	JobVolenteer jj = new JobVolenteer();
	 BarChartModel barModel ;
	
	@PostConstruct
	public void init(){
		us1= (AppUser)session.getAttribute("user");
		jobs=js.findAll(JobVolenteer.class);
	}
	
	public void hetJobss(){
		jobs=js.findAll(JobVolenteer.class);
		
	}
	
	
	public List<JobVolenteer> doSearchJob(String x)
	
	{ 
		System.out.println("lllllllllllllllllllllllllloooooooooooooooooooooo       p:"+x);

		
		if(x==""){
			ll=jobs ;
			System.out.println("aaaaaaaaaaaaaaaaaaaaaywah");
			x="";
		}
		else
		{
			ll=js.SearchJobVolenteer(x);
			x=null;
			System.out.println("opopopopopopoopopooo"+x);
		}
		
		return ll ;
	}
	
	public void doCreateJob() {
		
	//	System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"+ui.getFileName());
		/*String fileName = uploadedFile.getFileName();
		String contentType = uploadedFile.getContentType();
		System.out.println("fileName : " + fileName + " contentType : " + contentType);
		byte[] contents = uploadedFile.getContents();
		this.setPicture(contents);*/
		
		//SimpleDateFormat simpFormat = new SimpleDateFormat("dd-MM-yyyy");
		//String navigateTo = "/todos?faces-redirect=true";
		j.setTitre(Titre);
		//j.setDateDebut(DateDebut);
		//j.setDateFin(DateFin);
		
		j.setDateDebut(DateDebut);
		j.setDateFin(DateFin);
		j.setLieu(Lieu);
		j.setDescription(Description);
		j.setTaches(Taches);
		//j.setPicture(name);
		j.setImage(name);
		
		//if ((j.getDateFin().before(j.getDateDebut()))&&(j.getDateFin().before(new Date())))
		if (j.getDateFin().before(j.getDateDebut()))
		{
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error","please check your dates"));	
		}
		else
		{
		js.add(j);
		
		}
		hetJobss();
		js.send();
	
	}
	
	
	public void gotoupdateJOB(String i) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("updateJOB.jsf");
		int j = Integer.parseInt(i) ;
		JobVolenteer v = js.findById(JobVolenteer.class, j);
		idJobV1=v.getIdJobV();
		Titre1=v.getTitre();
		Lieu1=v.getLieu();
		Description1=v.getDescription();
		Taches1=v.getTaches();
		DateDebut1=v.getDateDebut();
		
		
	
	}
	public void updateJOB(String i)throws IOException {
		int j = Integer.parseInt(i) ;
		JobVolenteer v = js.findById(JobVolenteer.class, j);
		
		v.setTitre(Titre1);
		
		v.setLieu(Lieu1);
		v.setDescription(Description1);
		v.setTaches(Taches1);	
		js.update(v);
		FacesContext.getCurrentInstance().getExternalContext().redirect("showjobsAdmin.jsf");
		hetJobss();
		
		
	
	}
	public void DeleteJOB(String i) throws IOException{
		int j = Integer.parseInt(i) ;
		JobVolenteer v = js.findById(JobVolenteer.class, j);
		
		js.removeById(JobVolenteer.class, j);
		FacesContext.getCurrentInstance().getExternalContext().redirect("showjobsAdmin.jsf");

		hetJobss();
		
		
	
	}
	public void bbJOB() throws IOException{
		//List<Appliement> po = pew.getBestVolenteerUser();
	//	System.out.println("*****************************************"+po.size());
		

		int s=0 ;
		Map<AppUser,Integer> po =pew.getBestVolenteerUser();
		for (AppUser mapKey : po.keySet()) {
			// utilise ici hashMap.get(mapKey) pour accéder aux valeurs
			System.out.println("ffffffffffffffffffff "+po.get(mapKey));
			s=po.get(mapKey);
			
		}
		List<JobVolenteer> ps= pew.allparticipation();
		int l =ps.size();
		barModel = new BarChartModel();
		 ChartSeries boys = new ChartSeries();
	        boys.setLabel("Boys");
	        boys.set("Jobs", l);
	       
	 
	        ChartSeries girls = new ChartSeries();
	        girls.setLabel("max Jobs made by single user");
	        girls.set("Jobs",s);
	        
	        barModel.addSeries(boys);
	        barModel.addSeries(girls);
	        barModel.setTitle("Bar Chart");
	        barModel.setLegendPosition("ne");
	         
	        Axis xAxis = barModel.getAxis(AxisType.X);
	        xAxis.setLabel("jobs");
	         
	        Axis yAxis = barModel.getAxis(AxisType.Y);
	        yAxis.setLabel("quantity");
	        yAxis.setMin(0);
	        yAxis.setMax(10);
		
	        FacesContext.getCurrentInstance().getExternalContext().redirect("piechart.jsf");
	}
	
	public List<JobVolenteer> searchJobVolenter(String param)
	{
		return js.SearchJobVolenteer(param);
	}
	

	
	
	
	
	
	public void handleFileUpload(FileUploadEvent event) {
		name= event.getFile()
				.getFileName();
		FacesMessage message = new FacesMessage("Succesful", event.getFile()
				.getFileName() + " is uploaded.");
		FacesContext.getCurrentInstance().addMessage(null, message);
	
		
	//	C:\Users\aloulou\workspaceTest\backup\EasyMission\EasyMission\EasyMission-web\src\main\webapp\ressources\img
	
		
		//C:\Users\aloulou\workspaceTest\backup\EasyMission\EasyMission\EasyMission-web\src\main\webapp\ressources\img	
		String localPath = "C:" 
		        + File.separator + "Users"
				+ File.separator+ "aloulou" 
				+ File.separator + "workspaceTest" 
				+ File.separator + "backup" 
				+ File.separator + "EasyMission"
				+ File.separator + "EasyMission"
				+ File.separator + "EasyMission-web"
				+ File.separator + "src" 
				+ File.separator + "main"
				+ File.separator + "webapp" 
				+ File.separator + "ressources"
				+ File.separator + "img" 
				+ File.separator + name ;


	
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		String filepath = externalContext.getRealPath("") + File.separator
				+ "ressources" + File.separator + "img" + File.separator + name ;

		try (

		InputStream input = event.getFile().getInputstream()) {
			OutputStream os = new FileOutputStream(localPath);
			OutputStream osServer = new FileOutputStream(localPath);
			byte[] b = new byte[2048];
			int length;

			while ((length = input.read(b)) != -1) {
				os.write(b, 0, length);
				osServer.write(b, 0, length);
			}

			input.close();
			os.close();
			osServer.close();

		} catch (IOException e) {
			// Show faces message?
		}

	}


String name ;
    public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	private UploadedFile file;
	
	
	
	
	
	
	
	
	
	
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIdJobV1() {
		return idJobV1;
	}

	public void setIdJobV1(Integer idJobV1) {
		this.idJobV1 = idJobV1;
	}

	public String getTitre1() {
		return Titre1;
	}

	public void setTitre1(String titre1) {
		Titre1 = titre1;
	}

	public String getLieu1() {
		return Lieu1;
	}

	public void setLieu1(String lieu1) {
		Lieu1 = lieu1;
	}

	public String getDescription1() {
		return Description1;
	}

	public void setDescription1(String description1) {
		Description1 = description1;
	}

	public String getTaches1() {
		return Taches1;
	}

	public void setTaches1(String taches1) {
		Taches1 = taches1;
	}

	public IUserServiceLocal getUs() {
		return us;
	}

	public void setUs(IUserServiceLocal us) {
		this.us = us;
	}

	public List<User> getOo() {
		return oo;
	}

	public void setOo(List<User> oo) {
		this.oo = oo;
	}

	public JobVolenteer getJj() {
		return jj;
	}

	public void setJj(JobVolenteer jj) {
		this.jj = jj;
	}

	public List<JobVolenteer> getJobs() {
		return jobs;
	}

	public void setJobs(List<JobVolenteer> jobs) {
		this.jobs = jobs;
	}

	public IJobVolenteerServiceLocal getJs() {
		return js;
	}


	public void setJs(IJobVolenteerServiceLocal js) {
		this.js = js;
	}


	public Integer getIdJobV() {
		return idJobV;
	}

	public void setIdJobV(Integer idJobV) {
		this.idJobV = idJobV;
	}

	public String getTitre() {
		return Titre;
	}

	public void setTitre(String titre) {
		Titre = titre;
	}

	public Date getDateDebut() {
		return DateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		DateDebut = dateDebut;
	}

	public Date getDateFin() {
		return DateFin;
	}

	public void setDateFin(Date dateFin) {
		DateFin = dateFin;
	}

	public String getLieu() {
		return Lieu;
	}

	public void setLieu(String lieu) {
		Lieu = lieu;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getTaches() {
		return Taches;
	}

	public void setTaches(String taches) {
		Taches = taches;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public JobVolenteer getJ() {
		return j;
	}

	public void setJ(JobVolenteer j) {
		this.j = j;
	}

	public UploadedFile getUi() {
		return ui;
	}

	public void setUi(UploadedFile ui) {
		this.ui = ui;
	}

	public IAppliment<Appliement> getPew() {
		return pew;
	}

	public void setPew(IAppliment<Appliement> pew) {
		this.pew = pew;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<JobVolenteer> getLl() {
		return ll;
	}

	public void setLl(List<JobVolenteer> ll) {
		this.ll = ll;
	}

	public Date getDateDebut1() {
		return DateDebut1;
	}

	public void setDateDebut1(Date dateDebut1) {
		DateDebut1 = dateDebut1;
	}
	
	 public boolean resOrNot(int trainingId, int userId) {
	    	return applimentService.verifUserResOrNot(trainingId, userId);
	}
	    
	
	 public void cancelReservation(int trainingId){
	  	  applimentService.annulerReservation(trainingId,LoginBean.getUs().getId());
	  	}

}
