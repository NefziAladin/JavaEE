package tn.esprit.EasyMission.presentation.mbeans;

import java.awt.Event;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.EasyMission.Business.AppUserService;
import tn.esprit.EasyMission.Business.ApplimentService;
import tn.esprit.EasyMission.Business.JobVolenteerService;
import tn.esprit.EasyMission.Entity.Appliement;
import tn.esprit.EasyMission.Entity.JobVolenteer;

@ManagedBean
@ViewScoped
public class GrowlView {

	
	
	     
	    private String message;
	    
	    @EJB
	    ApplimentService  applimentService	 ;
	    
	    @EJB
		JobVolenteerService jobVolenteerService;
	   
	    @EJB
	    AppUserService userService;
	    
	    
	    JobBean jobBeanb = new JobBean();

	    LoginBean logbean = new LoginBean();
	 
	    public String getMessage() {
	        return message;
	    }
	 
	    public void setMessage(String message) {
	        this.message = message;
	    }
	     
	    
		
	    public boolean resOrNot(int trainingId, int userId) {
	    	return applimentService.verifUserResOrNot(trainingId, userId);
}
	    
	    
	    
	    public void saveMessage(JobVolenteer jobVolenteer ,int userId) {
	        FacesContext context = FacesContext.getCurrentInstance();
	         
	       
			Appliement appliemenNew = new Appliement();
		//	appliemenNew = applimentService.verifierAppliement(jobVolenteer,userService.getUsersById(userId));
			appliemenNew=applimentService.verifierAppliement(jobVolenteer,userService.getUsersById(userId));
			
			//applimentService.ver
			if(appliemenNew != null ){
		        context.addMessage(null, new FacesMessage("Error",  "You did already participate"+ message) );

					
			}
			else{
				
				applimentService.ajouterReservation(jobVolenteer.getIdJobV(), userId, new Date());
//				event.setNbParticipant(event.getNbParticipant()+1);
//				jobVolenteerService.editEvent(event);
				context.addMessage(null, new FacesMessage("Success",  "New Participation Saved"+ message) );
			}
	        
	    }
	   
	    
	    
	
}
